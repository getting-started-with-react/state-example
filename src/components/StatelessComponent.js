import React from 'react';

const StatelessComponent = (props) => 
{
    return (
        <h1>Test{props.developer}</h1>
    )
}

export default StatelessComponent;